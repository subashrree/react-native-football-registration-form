import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet,ScrollView,Picker,ImageBackground,Alert} from 'react-native'
import { RadioButton,Button} from 'react-native-paper';
import { Header} from 'react-native-elements';
class Inputs extends Component {
   state = {
      playername: '',
      playersagegroup: '',
      desiredteam: '',
      desiredposition: '',
      guardianname: '',
      guardianemail:'',
      guardianaddress:'',
      city:'',
      stateprovinceregion:'',
      postalzipcode:'',
      country:'',
      checked: 'male',
      checked1: 'male'
   }
    state = {  
        choosenIndex: 0  
    }; 
    state = {
    value: 'Jets',
  };
  state = {
    value: 'Running Back',
  };
  
  handleplayername = (text) => {
      this.setState({ playername: text })
   }
  
   
   handleplayersagegroup = (text) => {
      this.setState({ playersagegroup: text })
   }
   handledesiredteam = (text) => {
      this.setState({ desiredteam: text })
   }
   handledesiredposition = (text) => {
      this.setState({ desiredposition: text })
   }
   
   handleguardianname = (text) => {
      this.setState({ guardianname: text })
   }
   handleguardianemail = (text) => {
      this.setState({ guardianemail: text })
   }
    handleguardianaddress = (text) => {
      this.setState({ guardianaddress: text })
   }
   handlecity = (text) => {
      this.setState({ city: text })
   }
   handlestateprovinceregion = (text) => {
      this.setState({ stateprovinceregion: text })
   }
   handlepostalzipcode = (text) => {
      this.setState({ postalzipcode: text })
   }
   handlecountry = (text) => {
      this.setState({ country: text })
   }
   login = (playername,playersagegroup,desiredteam,desiredposition,guardianname,guardianemail,guardianaddress,city,stateprovinceregion,postalzipcode,country) => {
      alert( ' playername: ' + playername+'playersagegroup:'+playersagegroup+'desiredteam:'+desiredteam+'desiredposition:'+desiredposition+'guardianname:'+guardianname+'guardianemail:'+guardianemail+'guardianaddress:'+guardianaddress+'city'+city+'stateprovinceregion'+stateprovinceregion+'postalzipcode'+postalzipcode+'country'+country+'Desired Team:' + this.state.value1 + 'position:' + this.state.value2 )
   }
   render(){

     const {value1,value2}=this.state;
      
      return (
        
        <View style = {styles.container}>
        <ScrollView>
         <Header
          leftComponent={{
            icon: 'menu',
            color: 'white',
            onPress: () => alert('Registration is not completed'),
          }}
          centerComponent={{ text: 'FOOTBALL REGISTRATION', style: { color: 'white' } }}
          rightComponent={{ icon: 'home', color: 'white' }}
          backgroundColor="black"
        />
        
       
     <ImageBackground source={require('./components/f4.jpg')}
     
         style = {styles.container}>
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Player Name"
               placeholderTextColor = "black"
               autoCapitalize = "none"
               value={this.state.playername}
               onChangeText = {this.handleplayername }/>
            
        
               
            <TextInput style = {styles.choosenIndex}
               underlineColorAndroid = "transparent"
               placeholder = "Players Age Group"
               placeholderTextColor = "black"
               autoCapitalize = "none"
               value={this.state.playersagegroup}
               onChangeText = {this.handleplayersagegroup}/>
                 <Picker style={styles.pickerStyle}  
                        selectedValue={this.state.age}  
                        onValueChange={(itemValue, itemPosition) =>  
                            this.setState({age: itemValue, choosenIndex: itemPosition})}  
                    >  
                    <Picker.Item label="none" value="none" />
                    <Picker.Item label="6-8 years old" value="6-8 years old" />
                    <Picker.Item label="9-12 years old" value="9-12 years old" />  
                    <Picker.Item label="13-15 years old" value="13-15 years old" />  
                    <Picker.Item label="Above 15 years old" value="Above 15 years old" />  
                        
                </Picker> 
            <TextInput style = {styles.value}
               underlineColorAndroid = "transparent"
               placeholder = "Desired Team"
               placeholderTextColor = "black"
               autoCapitalize = "none"
               value={this.state.desiredteam}
               onChangeText = {this.handledesiredteam}/>
               <View style={{marginTop:10}}>
                <View style={{flexDirection:'row'}}>
            <RadioButton.Group
        onValueChange={value => this.setState({ value1:value })}
        value={value1}
      >

 <RadioButton value="Buccaneers" />
       
          <Text  style={{marginTop:10}}>Buccaneers </Text>
          
<RadioButton value="Jets" />
       
          <Text  style={{marginTop:10}} >Jets</Text>
<RadioButton value="Patriots " />
          <Text  style={{marginTop:10}}>Patriots </Text>
          
   <RadioButton value="Cowboys " />
          <Text  style={{marginTop:10}}>Cowboys </Text>
       
<RadioButton value="Redskins " />
          <Text  style={{marginTop:10}}>Redskins </Text>
          
        
      </RadioButton.Group>
      </View>
      </View>
      
                <TextInput style = {styles.value}
               underlineColorAndroid = "transparent"
               placeholder = "Desired Position"
               placeholderTextColor = "black"
               autoCapitalize = "none"
               value={this.state.desiredposition}
               onChangeText = {this.handledesiredposition}/>
                <View style={{marginTop:10}}>
                <View style={{flexDirection:'row'}}>
            <RadioButton.Group
        onValueChange={value => this.setState({ value2:value })}
        value={value2}
      >
        

          <RadioButton value="Quarterback " />
       
          <Text  style={{marginTop:10}} >Quarterback </Text>
                  <RadioButton value="Running Back  " />

          <Text  style={{marginTop:10}}>Running Back  </Text>
                 <RadioButton value="Wide Receiver  " />

          <Text  style={{marginTop:10}}>Wide Receiver  </Text>
       
         
      </RadioButton.Group>
       </View>
      </View>
       
       <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Guardian Name"
               placeholderTextColor = "black"
               value={this.state.guardianname}
               onChangeText = {this.handleguardianname}/>
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Guardian Email"
               placeholderTextColor = "black"
               autoCapitalize = "none"
               value={this.state.guardianemail}
               onChangeText = {this.handleguardianemail}/>
                           <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Guardian Address"
               placeholderTextColor = "black"
             
               value={this.state.guardianaddress}
               onChangeText = {this.handleguardianaddress}/>
            
              
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "CITY"
               placeholderTextColor = "black"
            
               value={this.state.handlecity}
               onChangeText = {this.handlecity}/>
            
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "State / Province / Region"
               placeholderTextColor = "black"
           
               value={this.state.stateprovinceregion}
               onChangeText = {this.handlestateprovinceregion}/>


                <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Postal / Zip Code"
               placeholderTextColor = "black"
              
               value={this.state.postalzipcode}
               onChangeText = {this.handlepostalzipcode}/>

                <TextInput style = {styles.choosenIndex}
               underlineColorAndroid = "transparent"
               placeholder = "Country"
               placeholderTextColor = "black"
              
               value={this.state.country}
               onChangeText = {this.handlecountry}/>

                              <Picker style={styles.pickerStyle}  
                        selectedValue={this.state.place}  
                        onValueChange={(itemValue, itemPosition) =>  
                            this.setState({place: itemValue, choosenIndex: itemPosition})}  
                    >  
                    <Picker.Item label="none" value="none" />
                    <Picker.Item label="America" value="America" />
                    <Picker.Item label="Bangladesh" value="Bangladesh" />  
                    <Picker.Item label="China" value="China" />  
                    <Picker.Item label="Dargling" value="Dargling" /> 
                    <Picker.Item label="Egypt" value="Egypt" />   
                    <Picker.Item label="Finland" value="Finland" />  
                    <Picker.Item label="Germany" value="Germany" />  
                    <Picker.Item label="Hong Kong" value="Hong Kong" />
                    <Picker.Item label="India" value="India" />    
                     
                        
                </Picker> 
            
            <Button
            mode="contained"
               style = {styles.submitButton}
               onPress = {
                  () => fetch('http://192.168.51.40:3001/suba/', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    Name: this.state.playername,
     Playersagegroup:this.state.age,
     Desiredteam:this.state.value1,
     Desiredposition:this.state.value2,
   Gname:this.state.guardianname,
   Gemail:this.state.guardianemail,
   Gaddress:this.state.guardianaddress,
   City:this.state.city,
   Province:this.state.stateprovinceregion,
   Postal:this.state.postalzipcode,
   Country:this.state.place,
  })
}).then(Alert.alert("Registered Successfully"))
.catch(err=>console.warn(err))
               }>
              <Text style={styles.tex}> SUBMIT </Text>
         
            </Button>
            
        
        </ImageBackground>
          </ScrollView>
           </View>
      )
   }
}
export default Inputs
const styles = StyleSheet.create({
   container: {
      paddingTop: 23
   },
   input: {
      margin: 15,
      height: 40,
      backgroundColor: '#ccc',
      borderColor: 'white',
      borderWidth: 1,
    
   },
   submitButton: {
      backgroundColor: 'black',
      padding: 10,
      margin: 15,
   },
   tex: {
     color: 'white',
     justifyContent: 'center',
   }
  
})